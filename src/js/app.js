import * as flsFunctions from "./modules/functions.js";
flsFunctions.isWebp();

const burgerLogo = document.querySelectorAll(".logo");
const burgerLog = document.querySelector(".burger__log");
const burgerMenu = document.querySelector(".nav__menu__burger");
let click = 0;
window.addEventListener("resize", () => {
  let windowWidth = window.innerWidth;
  if (windowWidth > 780) {
    burgerMenu.style.display = "none";
    burgerLog.style.display = "block";
    click = 0;
  }
});

burgerLogo.forEach((event) => {
  event.addEventListener("click", (icon) => {
    click++;
    if (click === 1) {
      burgerLog.style.display = "none";
      burgerMenu.style.display = "flex";
    } else if (click === 2) {
      burgerLog.style.display = "block";
      burgerMenu.style.display = "none";
      click = 0;
    }
  });
});
